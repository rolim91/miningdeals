from django import forms

class MailForm(forms.Form):

	item = forms.CharField(
		label="Item of Interest",
		widget=forms.TextInput(attrs={ 'required': 'true', 'placeholder': 'Product ID & Name*' })
		)

	name = forms.CharField(
		label="First Name", 
		max_length=100, 
		required=True,
		widget=forms.TextInput(attrs={ 'required': 'true', 'placeholder': 'Your Name*'})
		)

	email = forms.EmailField(
		label="Email",
		widget=forms.TextInput(attrs={ 'required': 'true', 'placeholder': 'Email*' })
		)

	phone = forms.CharField(
		label="Phone Number", 
		max_length=100, 
		widget=forms.TextInput(attrs={ 'required': 'true', 'placeholder': 'Phone Number*' })
		)


	message = forms.CharField(
		widget=forms.Textarea(attrs={ 'required': 'true', 'placeholder': 'Your Message*' }), 
		label="Message"
		)

	location = forms.CharField(
		label="Project Location", 
		widget=forms.TextInput(attrs={'placeholder': 'Location' })
		)


