from django import template

register = template.Library()

@register.filter(name='replaceSpaceUnderscore')
def replaceSpaceUnderscore(value):
	""" Replaces spaces with underscores """
	return value.replace(' ','_')

@register.filter(name='returnItemAtIndex')
def returnItemAtIndex(thisList, index):
	""" Returns the item in the list given an index """
	try:
		return thisList[index]
	except:
		return None