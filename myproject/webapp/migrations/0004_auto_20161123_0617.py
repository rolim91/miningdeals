# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import webapp.models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0003_auto_20161123_0602'),
    ]

    operations = [
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('picture', models.ImageField(null=True, upload_to=webapp.models.post_directory_path, blank=True)),
                ('product_id', models.ForeignKey(to='webapp.Product')),
            ],
        ),
        migrations.RemoveField(
            model_name='image',
            name='product_id',
        ),
        migrations.DeleteModel(
            name='Image',
        ),
    ]
