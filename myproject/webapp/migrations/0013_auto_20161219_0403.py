# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0012_auto_20161218_0222'),
    ]

    operations = [
        migrations.AddField(
            model_name='webinfo',
            name='disclaimer_content',
            field=models.TextField(default=datetime.datetime(2016, 12, 19, 4, 3, 36, 762926, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='webinfo',
            name='privacy_content',
            field=models.TextField(default=datetime.datetime(2016, 12, 19, 4, 3, 39, 770437, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='webinfo',
            name='terms_content',
            field=models.TextField(default=datetime.datetime(2016, 12, 19, 4, 3, 41, 464376, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
