# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0011_webinfo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webinfo',
            name='home_description',
            field=models.TextField(),
        ),
    ]
