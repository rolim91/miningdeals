# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0009_auto_20161217_2215'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='item_number',
            field=models.CharField(default='0000', max_length=100),
        ),
    ]
