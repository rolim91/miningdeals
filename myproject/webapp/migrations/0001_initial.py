# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import webapp.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('cat_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('product_id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('picture', models.ImageField(null=True, upload_to=webapp.models.post_directory_path, blank=True)),
                ('descriptoin', models.TextField()),
                ('quantity', models.IntegerField()),
                ('cat_id', models.ForeignKey(to='webapp.Category')),
            ],
        ),
    ]
