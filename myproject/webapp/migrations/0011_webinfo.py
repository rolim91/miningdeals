# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0010_auto_20161218_0023'),
    ]

    operations = [
        migrations.CreateModel(
            name='Webinfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('home_description', models.CharField(max_length=4000)),
            ],
        ),
    ]
