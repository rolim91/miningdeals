from django.conf.urls import url
from django.contrib.sitemaps.views import sitemap
from sitemap import ProductSitemap, CategorySitemap, ViewSitemap

from . import views

sitemaps = {
	'product': ProductSitemap,
	'category': CategorySitemap,
	'static': ViewSitemap,
}

app_name = 'webapp'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^terms/$', views.terms, name='terms'),
    url(r'^policy/$', views.policy, name='policy'),
    url(r'^disclaimer/$', views.disclaimer, name='disclaimer'),
    url(r'^category/$', views.category, name='category'),
    url(r'^category/(?P<cat_id>[0-9]+)/$', views.category_view, name='category_view'),
    url(r'^product/(?P<product_id>[0-9]+)/$', 
    	views.product, name='product'),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
]


