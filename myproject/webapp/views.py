from webapp.util.page_util import *

from django.conf import settings
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from webapp.settings import *
from webapp.util.util import *
from .forms import MailForm
from urllib import urlencode
from webapp.models import Webinfo
from django.contrib.sitemaps import Sitemap



# Create your views here.

def index(request):
	"""
	Home page of the web app
	"""

	home_info = Webinfo.objects.all()[0]

	#Header
	#Package context
	context = {
		'current':'home',
		'title':'New Products On Sale',
		'category_list':getCategories(),
		'home_description': home_info,
	}



	return render(request, 'webapp/index.html', context)


def terms(request):
	"""
	Home page of the web app
	"""

	content = Webinfo.objects.all()[0].terms_content

	#Header
	#Package context
	context = {
		'current':'home',
		'title':'New Products On Sale',
		'category_list':getCategories(),
		'content': content,
	}



	return render(request, 'webapp/generic.html', context)


def policy(request):
	"""
	Home page of the web app
	"""

	content = Webinfo.objects.all()[0].privacy_content

	#Header
	#Package context
	context = {
		'current':'home',
		'title':'New Products On Sale',
		'category_list':getCategories(),
		'content': content,
	}



	return render(request, 'webapp/generic.html', context)


def disclaimer(request):
	"""
	Home page of the web app
	"""

	content = Webinfo.objects.all()[0].disclaimer_content

	#Header
	#Package context
	context = {
		'current':'home',
		'title':'New Products On Sale',
		'category_list':getCategories(),
		'content': content,
	}



	return render(request, 'webapp/generic.html', context)

def contact(request):
	"""
	Contact page of the web app
	"""

	context = {
		'current':'contact',
		'title':'Contact Us',
		'category_list':getCategories(),
	}

	# If POST request
	if request.method == 'POST':

		form = MailForm(data=request.POST)

		if form.is_valid():

			#Email the posted information to SMTP
			name = form.cleaned_data['name']
			phone = form.cleaned_data['phone']
			email = form.cleaned_data['email']
			item = form.cleaned_data['item']
			message = form.cleaned_data['message']
			location = form.cleaned_data['location']

			#Package data then email the message to smtp email
			try:
				subject = "Request for information of item %s" % item
				send_message = "Name: %s\nPhone number: %s\nItem: %s\nLocation: %s \nEmail: %s\n\nMessage: %s\n" % (name, phone, item, location, email, message)
				send_mail(subject, send_message, email, [settings.EMAIL_FORWARD])
			
			except BadHeaderError:
				return render(request, 'webapp/error.html', context)

			return render(request, 'webapp/thanks.html', context)
		else:
			return render(request, 'webapp/error.html', context)
	
	# If GET request
	else:
		item = request.GET.get("item", "")
		item = item.replace("_", " ")
		form = MailForm(initial={'item': item})

	context['form'] = form 


	return render(request, 'webapp/contact.html', context)


def category(request):
	"""
	Category page of the web app
	"""

	return redirect( reverse('webapp:category_view', 
		kwargs={'cat_id':0} )) 


def category_view(request, cat_id):
	"""
	Category page of the web app

	Keyword arguments:
	cat_name -- the name of the category
	cat_id -- the category if of the category
	"""

	#helper variables
	cat_id_int = int(cat_id)
	name = "All Products"
	page_num_int = 1
	search_url = ''
	searchpath = ''
	post_list = None
	post_info = None
	check_sold = Q(sold=False)

	#check GET request

	if 'page' in request.GET and request.GET['page'] != '':
		page_num_int = int(request.GET['page'])
		print page_num_int

	if 'search' in request.GET and request.GET['search'] != '':

		name = 'Search'
		search_terms = request.GET['search']

		#Get url version of search values
		#remove page from request.GET
		param_temporary = request.GET.copy()
		if 'page' in param_temporary:
			del param_temporary['page']
		search_url = urlencode(param_temporary).split("=")[-1]

		#Query for search and filter resulting value
		query = get_query(search_terms, ['name', 'description', 'item_number'])
		post_list = Product.objects.filter(check_sold, query) 


		#Get the pages
		num_of_pages = getPages(post_list.count(), MAX_POSTS_PER_PAGE)
		paging_info = generatePagingInfo(page_num_int, num_of_pages, 
			MAX_PAGES_TO_SHOW)

		#redirect to last page if page_num > num_of_pages
		if post_list and page_num_int > num_of_pages:
			redirect_url = "/%s", num_of_pages
			return HttpResponseRedirect('/category/0/?search=%s' % search_terms)

	else:

		if cat_id_int == 0:
			post_list = Product.objects.filter(check_sold)
		else:
			cat_object = get_object_or_404(Category, cat_id=cat_id)
			name = cat_object.name
			post_list = Product.objects.filter(check_sold, categories__pk=cat_id)

		#Get the pages
		num_of_pages = getPages(post_list.count(), MAX_POSTS_PER_PAGE)
		paging_info = generatePagingInfo(page_num_int, num_of_pages, 
			MAX_PAGES_TO_SHOW)

		#redirect to last page if page_num > num_of_pages
		if post_list and page_num_int > num_of_pages:
			redirect_url = "/%s", num_of_pages
			return HttpResponseRedirect('/category/%s' % cat_id)

	if post_list:
		post_info = getPostInPage(page_num_int, MAX_POSTS_PER_PAGE, post_list)

	#Package context
	context = {
	'current':'category',
	'title':name,
	'category': cat_id,
	'latest_post_list': post_info,
	'url_var': "webapp:category_view",
	'category_list':getCategories(), 
	'search_terms': search_url,
	}

	context.update(paging_info)


	return render(request, 'webapp/post_list.html', context)


def product(request, product_id):
	"""
	Post page of the web app

	Keyword arguments:
	cat_id -- the category if of the category
	post_id -- the id of the post
	"""
	#Get web post and check if post exists. If it does not 404
	product = get_object_or_404(Product, product_id=product_id)
	cat_id = 0;

	if 'cat' in request.GET and request.GET['cat'] != '':
		cat_id = int(request.GET['cat'])
		print cat_id


	if cat_id != 0:
		category = product.categories.get(cat_id=cat_id)
		if not category:
			raise Http404("Product in that category does not exist")
		else:
			category_name=category.name
	else:
		category_name='All Products'


	context = {
		'current':'product',
		'category_name':category_name,
		'cat_id':cat_id,
		'category_list':getCategories(),
		'product': product,
	}

	return render(request, 'webapp/product.html', context)


def getCategories():
	"""
	Helper function to obtain categories available
	"""
	category_list = getAllCategories() #Get All Categories
	category_index = range(len(category_list))
	category_combined = zip(category_list, category_index)

	return category_combined

class ProductSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Product.objects.all()