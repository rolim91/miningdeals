from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from .views import Product, Category
import datetime

class ViewSitemap(Sitemap):
    changefreq = 'daily'
    priority = 0.5

    def items(self):
        return ['webapp:index', 'webapp:contact', 'webapp:terms', 'webapp:policy', 'webapp:disclaimer', 'webapp:category']

    def location(self, item):
        return reverse(item)


class ProductSitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Product.objects.all()

    def lastmod(self, obj):
        return datetime.datetime.now()

class CategorySitemap(Sitemap):
    changefreq = "daily"
    priority = 0.5

    def items(self):
        return Category.objects.all()

    def lastmod(self, obj):
        return datetime.datetime.now()