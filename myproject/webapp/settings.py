from django.conf import settings

#Number of Posts per page
MAX_POSTS_PER_PAGE = getattr(settings, 'MAX_POSTS_PER_PAGE', 8.0)
MAX_POSTS_PER_PAGE_CATEGORY_MAIN = getattr(settings, 'MAX_POSTS_PER_PAGE_CATEGORY_MAIN', 8)
MAX_POSTS_PER_PAGE_CATEGORY_INDIV = getattr(settings, 'MAX_POSTS_PER_PAGE_CATEGORY_INDIV', 8)
MAX_PAGES_TO_SHOW = getattr(settings, 'MAX_PAGES_TO_SHOW', 5)