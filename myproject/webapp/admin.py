from django.contrib import admin
from .models import Category, Product, Picture, Webinfo

# Register your models here.

class PictureInline (admin.StackedInline):
    """Inline configuration for Django's admin on the Product model."""
    model = Picture
    extra = 1

class ProductAdmin (admin.ModelAdmin):
	filter_horizontal = ('categories', )
	inlines = [ PictureInline ]
	list_display = ('item_number', 'name')

admin.site.register(Product, ProductAdmin)
admin.site.register(Category)
admin.site.register(Webinfo)
