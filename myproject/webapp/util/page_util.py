from math import ceil

def getPages(current_amount, amount_per_page):
	'''
	getPages

	Get the amount of pages given the amount given and amount per page.
	'''
	return int(ceil(current_amount/amount_per_page))


def getPagesToShow(current_page, number_of_pages, max_pages_to_show):
	'''
	getPagesToShow

	Get the pages to show (used for pagination)
	'''

	start_at = current_page - (max_pages_to_show / 2)
	if start_at < 1:
		start_at = 1

	end_at = start_at + (max_pages_to_show - 1)
	if end_at > number_of_pages and start_at == 1:
		end_at = number_of_pages
	elif end_at > number_of_pages:
		end_at = number_of_pages
		start_at = end_at - (max_pages_to_show - 1)

	return range(start_at, end_at + 1)

def generatePagingInfo(current_page, number_of_pages, max_pages_to_show):
	'''
	generatePagingInfo

	Get the pages to show (used for pagination)

	return a dictionary of paging info needed to create pagination
	'''

	pages = getPagesToShow(current_page, number_of_pages, max_pages_to_show)

	#Generate Paging Dictionary
	paging_info = {
	'pages': pages,
	'first_page':1,
	'current_page':current_page,
	'previous_page':current_page - 1,
	'next_page':current_page + 1,
	'last_page':number_of_pages
	}

	return paging_info
