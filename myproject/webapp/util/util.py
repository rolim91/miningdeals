import re

from webapp.models import Product, Category, Picture
from django.core.mail import send_mail
from django.db.models import Q

def getPostList(page_num, max_post_per_page):
	'''
	getPostList in alphabetical order.
	function will get posts depending on page.
	'''
	start_post_position = (page_num - 1) * max_post_per_page
	end_post_position = start_post_position + max_post_per_page
	return Product.objects.order_by('name')[start_post_position:end_post_position]

def getPostListPerCategory(page_num, max_post_per_page, id):
	'''
	getPostListPerCategory in alphabetical order.
	function will get posts depending on page and category
	'''
	start_post_position = (page_num - 1) * max_post_per_page
	end_post_position = start_post_position + max_post_per_page
	category_objects = Product.objects.filter(categories__pk=id).order_by('name')[start_post_position:end_post_position]
	return category_objects

def getPostListPerCategoryFilter(page_num, max_post_per_page, id, filter):
	'''
	getPostListPerCategoryFilter in alphabetical order.
	function will get posts depending on page and category and filter given
	'''
	start_post_position = (page_num - 1) * max_post_per_page
	end_post_position = start_post_position + max_post_per_page
	category_objects = Product.objects.filter(filter).order_by('name')[start_post_position:end_post_position]
	return category_objects

def getPostInPage(page_num, max_post_per_page, posts):
	'''
	getPostInPage in alphabetical order.
	function will get posts depending on page and category and filter given
	'''
	start_post_position = (page_num - 1) * max_post_per_page
	end_post_position = start_post_position + max_post_per_page
	return posts.order_by('name')[start_post_position:end_post_position]


def getAllCategories():
	return Category.objects.all()

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):

    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)] 

def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.
    
    '''
    query = None # Query to search for every search term        
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query