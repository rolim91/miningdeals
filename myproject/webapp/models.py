from __future__ import unicode_literals

from django.db import models
from uuid import uuid4

# Create your models here.
def post_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid4().hex, ext)

    product_name = instance.product_id.name.replace(" ", "_")
    product_name = product_name.replace("/", "")

    return 'webapp/images/product_{0}/{1}'.format(product_name, filename)


class Category(models.Model):
    '''
    Catogory Model
    ManyToMany relationship with Product
    cat_id - index of the Catogory
    name - name of the Catogory
    '''
    cat_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"

    def get_absolute_url(self):
        return "/category/%i/" % self.cat_id


class Product(models.Model):
    product_id = models.AutoField(primary_key=True)
    item_number = models.CharField(max_length=100, default="0000")
    name = models.CharField(max_length=200)
    categories = models.ManyToManyField(Category, related_name='categories')
    description = models.TextField()
    quantity = models.IntegerField()
    price = models.CharField(max_length=100, default="Request for Quote")
    sold = models.BooleanField(default=False)

    def __str__(self):
        return "%s %s" % (self.item_number, self.name)

    def get_absolute_url(self):
        return "/product/%i/" % self.product_id




class Picture(models.Model):
    '''
    Picture Model
    ManyToOne relationship with Product
    '''
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    picture = models.ImageField(upload_to=post_directory_path, blank=False, null=True)


class Webinfo(models.Model):
    '''
    Webinfo
    Information of Web
    '''
    home_description = models.TextField()
    terms_content = models.TextField()
    privacy_content = models.TextField()
    disclaimer_content = models.TextField()

    class Meta:
        verbose_name_plural = "Webinfo"
