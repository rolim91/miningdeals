
from webapp.models import Product, Category
import xlrd


# product = Product(item_number=1, name='Product 8', description="This is product 8", quantity=10, price="$10.00", sold=True)
# product.save()

wb = xlrd.open_workbook('used_equipment.xlsx')

sheet = wb.sheets()[0]
num_of_rows = sheet.nrows
num_of_cols = sheet.ncols

for row in range(3, num_of_rows):

	item_number = str(sheet.cell(row,2).value)
	name = str(sheet.cell(row,3).value)
	description = str(sheet.cell(row,4).value)
	price = str(sheet.cell(row,5).value)

	tempQuantity = str(sheet.cell(row,7).value)

	if tempQuantity != '':
		quantity = int(float(tempQuantity))
	else:
		quantity = 0

	tempSold = str(sheet.cell(row,15).value)

	if tempSold == 'Yes':
		sold = True
	else:
		sold = False

	print "item_number: %s" % item_number
	print "name: %s" % name
	print "description: %s" % description
	print "price: %s" % price
	print "quantity: %s" % quantity
	print "sold: %s" % sold

	if name != '':
		product = Product(item_number=item_number, name=name, description=description, quantity=quantity, price=price, sold=sold)
		product.save()

	# for col in range(num_of_cols):
	# 	print sheet.cell(row,col).value